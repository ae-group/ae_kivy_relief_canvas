<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# kivy_relief_canvas 0.3.12

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_relief_canvas/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_relief_canvas)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_relief_canvas/release0.3.11?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_relief_canvas/-/tree/release0.3.11)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_relief_canvas)](
    https://pypi.org/project/ae-kivy-relief-canvas/#history)

>ae_kivy_relief_canvas module 0.3.12.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_relief_canvas/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_relief_canvas/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_relief_canvas/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_relief_canvas/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_relief_canvas/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_relief_canvas/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_relief_canvas)](
    https://gitlab.com/ae-group/ae_kivy_relief_canvas/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_relief_canvas)](
    https://gitlab.com/ae-group/ae_kivy_relief_canvas/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_relief_canvas)](
    https://gitlab.com/ae-group/ae_kivy_relief_canvas/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_relief_canvas)](
    https://pypi.org/project/ae-kivy-relief-canvas/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_relief_canvas)](
    https://gitlab.com/ae-group/ae_kivy_relief_canvas/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_relief_canvas)](
    https://libraries.io/pypi/ae-kivy-relief-canvas)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_relief_canvas)](
    https://pypi.org/project/ae-kivy-relief-canvas/#files)


## installation


execute the following command to install the
ae.kivy_relief_canvas module
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-relief-canvas
```

if you want to contribute to this portion then first fork
[the ae_kivy_relief_canvas repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_relief_canvas "ae.kivy_relief_canvas code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_relief_canvas):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_relief_canvas/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_relief_canvas.html
"ae_kivy_relief_canvas documentation").
